import * as CSV from 'papaparse'
import $ from 'cash-dom'
import { OutputItem, RendererApi, RendererContext } from 'vscode-notebook-renderer'

export function activate(ctx: RendererContext<unknown>): RendererApi {
    return {
        renderOutputItem: render,
        disposeOutputItem(id?: string) {
            if (id) {
                $(`style[output-id="${id}"]`).remove()
            } else {
                $(`style.simple-csv[output-id]`).remove()
            }
        }
    }
}

function render(item: OutputItem, element: HTMLElement) {
    if (item.mime != 'text/csv') {
        return
    }

    const { data } = CSV.parse<string[]>(item.text())
    if (!data.length) return

    let style = $(`style[output-id="${item.id}"]`)
    if (!style.length)
        style = $(`<style output-id="${item.id}" class="simple-csv">`).appendTo('head')
    style.text(`
        [id="${item.id}"].output main {
            overflow: auto;
            white-space: nowrap;
            max-height: 40em;
        }

        [id="${item.id}"].output th {
            position: sticky;
            top: 0;
            background: var(--vscode-notebook-outputContainerBackgroundColor);
        }
    `)

    const wrapper = $('<main>').appendTo(element)
    const table = $('<table>').appendTo(wrapper)

    const header = data[0]
    if (header.every(x => x.match(/^[\w ]+$/)) && new Set(header).size == header.length) {
        data.splice(0, 1)

        const tr = $('<tr>').appendTo($('<thead>').appendTo(table))
        header.forEach(v => $('<th>').appendTo(tr).append($('<div>').text(v)))
    }

    const body = $('<tbody>').appendTo(table)

    for (const row of data) {
        if (!row.some(x => x.length > 0))
            continue

        const tr = $('<tr>').appendTo(body)
        row.forEach(v => $('<td>').appendTo(tr).text(v))
    }

    requestAnimationFrame(() => {
        const { backgroundColor } = getComputedStyle(table.find('thead')[0]!)
        const { padding } = getComputedStyle(table.find('th')[0]!)

        style[0]!.innerHTML += `
            [id="${item.id}"].output th {
                padding: 0;
            }
            [id="${item.id}"].output th > div {
                padding: ${padding};
                background-color: ${backgroundColor};
            }
        `
    })
}