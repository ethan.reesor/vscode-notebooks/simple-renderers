import hljs from 'highlight.js/lib/core'
import xml from 'highlight.js/lib/languages/xml'

import $ from 'cash-dom'
import css from 'highlight.js/styles/dark.css'
import { OutputItem, RendererApi, RendererContext } from 'vscode-notebook-renderer'

let cssLoaded = false

export function activate(ctx: RendererContext<unknown>): RendererApi {
    return {
        renderOutputItem: render
    }
}

const registered = new Set<(hljs?: HLJSApi) => LanguageDetail>()

const languages: Record<string, 'xml' | 'html'> = {
    'text/xml': 'xml',
    'application/xml': 'xml',
    'text/html': 'html',
}

const highlighters = { xml, html: xml }

function render(item: OutputItem, element: HTMLElement) {
    if (!(item.mime in languages))
        return

    if (!cssLoaded) {
        if ($('style#highlight-js').length == 0)
            $('<style>').attr('id', 'highlight-js').text(css).appendTo('head')
        cssLoaded = true
    }

    const language = languages[item.mime]
    const highlighter = highlighters[language]
    if (!registered.has(highlighter)) {
        hljs.registerLanguage(language, highlighter)
        registered.add(highlighter)
    }

    const { value } = hljs.highlight(item.text(), { language })

    $('<pre>').html(value).css({
        background: 'var(--vscode-editor-background)',
        overflow: 'auto',
    }).appendTo(element)
}