declare module 'highlight.js/styles/*.css' {
    const css: string;
    export default css;
}