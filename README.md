# Simple Notebook Renderers for VSCode

## CSV

Renders `text/csv` as a scrollable `<table>` with sticky headers. Uses
[Papaparse](https://www.papaparse.com/) for CSV parsing.

![simple csv](images/simple-csv.png)

## Highlight.js

Renders code with `highlight.js`. Currently supports:

- `text/xml`, `application/xml`

![hljs xml](images/hljs-xml.png)